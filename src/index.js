import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import * as firebase from 'firebase';

 // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAt0Y6E1tiznCi146lgvAP7AFYe2tQYACg",
    authDomain: "nutrinow-64976.firebaseapp.com",
    databaseURL: "https://nutrinow-64976.firebaseio.com",
    projectId: "nutrinow-64976",
    storageBucket: "nutrinow-64976.appspot.com",
    messagingSenderId: "617205916350"
  };
  
  firebase.initializeApp(config);


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
