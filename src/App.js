import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as firebase from 'firebase';

class App extends Component {

  constructor(){
    super()
    this.state = {
      speed: 15
    };
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const self = this; 
    const rootRef = firebase.database().ref();
    const speedRef = rootRef.child('react/speed');
    speedRef.on('value', snap => { 
      this.setState({
            speed: snap.val()
          });
    });
  }

  handleClick() {
    const rootRef = firebase.database().ref().child('react'); 
    rootRef.push({
           speed: 50 
          });
  };


   createPatient() { 
    const rootRef = firebase.database().ref().child('patients');
    // const speedRef = rootRef.child('speed');
    rootRef.push({
      birthday : 1498323281,
			fullName : 'testName',
			email : 'email@nutrinow.com',
			gender : 'M',
			phone : 0,
			rating: 0,
			photoPerfilURL : 'http://',
			bodyMeasurements : {
				timestamp_1498323281 : true,
				timestamp_1498323282 : true,
				timestamp_1498323283 : true
			},
			meals : {  
				mealId_1: true
			}

    });
  };

  updatePatient() { 
    const patientId = 'patientId';
    const rootRef = firebase.database().ref().child('patients/' + patientId); 
    rootRef.update({ 
			fullName : 'testName' 
    });
  };


  render() { 
    return (
      <div className="App">
        <h1>{this.state.speed}</h1>
        <button onClick={this.handleClick}> 
          Click Me!
        </button> 
        <br/> 
        <br/>
        <div> 
          <button onClick={this.createPatient}> 
            New Patient!
          </button>
        </div>
        <br/> 
        <div> 
          <button onClick={this.updatePatient}> 
            Update Patient!
          </button>
        </div>
      </div>
    );
  }
}

export default App;
