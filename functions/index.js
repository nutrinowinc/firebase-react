
// Import the Firebase SDK for Google Cloud Functions.
const functions = require('firebase-functions');

// Import and initialize the Firebase Admin SDK.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

/**
 * When a Meal is saved/created, should trigger Functions to add 
 * this Meal in the last position of the Unreviewed Meals' List.
 */

exports.addMealToUnreviewedMeals = 
functions.database.ref('/meals/{mealId}').onWrite(event => {
  
  const snapshot = event.data;

  // Only edit data when it is first created.
  if (snapshot.previous.exists()) {
    console.log("{addMealToUnreviewedMealsList}: Data already exists!"); 
    return;
  }
  // Exit when the data is deleted.
  if (!snapshot.exists()) {
    console.log("{addMealToUnreviewedMealsList}: Data is deleted!");  
    return;
  }
 
  const id = snapshot.key;

  const payload = {
    id: `${id}`
  };
 console.log("snapshot id: " + id);  
 
 return admin.database().ref('unreviewedMeals').push(payload);
});

/**
 * When a Nutritionist is available, should trigger Functions 
 * to add this Professional in the last position of the Reviewer's List
 * 
 * OR
 * 
 * When a Nutritionist is unavailable, should trigger Functions 
 * to remove this Professional from the Reviewer's List
 */
 
exports.addAvailableNutritionistToReviewerList = 
functions.database.ref('/nutritionists/{nutritionistId}/status').onWrite(event => {
  
  const snapshot = event.data;

  // Only edit data when it already exists.
  if (!snapshot.previous.exists()) {
    console.log("{addAvailableNutritionistToReviewerList}: Data doesn't exists!"); 
    return;
  }
  // Exit when the data is deleted.
  if (!snapshot.exists()) {
    console.log("{addAvailableNutritionistToReviewerList}: Data is deleted!");  
    return;
  }

  const status = snapshot.val();

  const id = event.params.nutritionistId;

  const payload = {
    id: `${id}`
  };

 console.log("snapshot key: " + id);  
 console.log("snapshot status: " + status)

  if (status == "available"){ 
    console.log("add to DB : " + id); 
    return admin.database().ref('reviewers').push(payload); 
  } else {  
    return admin.database().ref("reviewers").orderByChild("id").equalTo(id)
        .once("child_added", function(childSnapshot) {  
            console.log("reviewer key : " + childSnapshot.key);  
            console.log("reviewer id: " + childSnapshot.val().id);  
            if(childSnapshot.val().id === id) {
             console.log("remove from DB : " + childSnapshot.key); 
             return  admin.database().ref('reviewers').child(childSnapshot.key).remove();
            }
    });
  } 
});


/**
 * When a Nutritionist finished a review, should trigger Functions to notify the Patient 
 * by PushNotification and should put the Professional in the last position of the Reviewer's list.
 */

exports.onMealReviewedNotifyPatient = 
functions.database.ref('/reviewedMeals/{mealId}').onWrite(event => {
  
  const snapshot = event.data;

  // Exit when the data is deleted.
  if (!snapshot.exists()) {
    console.log("{addMealToUnreviewedMealsList}: Data is deleted!");  
    return;
  }
 
  const key = snapshot.key;
  const professionalId = snapshot.val().reviewedBy;

  console.log("snapshot key: " + key);  
  console.log("snapshot id: " + snapshot.val().id);  
  console.log("professionalId: " + professionalId); 

// Get Meal from "meals" node and change "status" to reviewed and "reviewedBy" with Nutritionist ID
return admin.database().ref("meals").orderByKey().equalTo(key)
    .once("child_added", function(childSnapshot) {  
        console.log("meal key : " + childSnapshot.key);
        const patientKey =  childSnapshot.val().patient;
        console.log("patient key: " + patientKey);  
        var postData = {
          reviewed: true
        };
        admin.database().ref("meals/"+childSnapshot.key).update(postData);

        // Get Professional and add to the last position of the Reviewer's List
        return admin.database().ref("reviewers").orderByChild("id").equalTo(professionalId)
            .once("child_added", function(childSnapshot) {  
                const reviewerKey = childSnapshot.key
                console.log("reviewer key : " + reviewerKey);   
                const reviewerId =  childSnapshot.val().id; 
                if(reviewerId === professionalId) {
                  // Get Meals, get Patient's ID and send a message by FCM 
                return admin.database().ref("patients").orderByKey().equalTo(patientKey)
                    .once("child_added", function(childSnapshot) { 
                      //TODO: Send push notification for this patients 
                        console.log("patient FCM-Token: " + childSnapshot.val().fcm_token); 
              
                        // Send notifications to all tokens.
                        // return admin.messaging().sendToDevice(childSnapshot.val().fcm_token, payload)

                        console.log("remove reviewer Key from DB : " + reviewerKey); 
                        admin.database().ref('reviewers').child(reviewerKey).remove();

                        var payload = {
                          id: `${professionalId}`
                        };
                        return admin.database().ref('reviewers').push(payload); 
                    }); 
                }
            });
    }); 
});


/**
 * 
 * When a Nutritionist is the first of Reviewer's list, 
 * should trigger Functions to notify this Professional by PushNotification.
 */ 
exports.notifyNutritionistOnFirstSpotOfReviewersList = 
functions.database.ref('/reviewers').onWrite(event => {
  const snapshot = event.data;   
  
  snapshot.forEach((result) => {  
    console.log("result: " + result.val().id);
    console.log("key: " + result.key); 
    admin.database().ref("nutritionists").orderByKey().equalTo(result.val().id)
        .once("child_added", function(childSnapshot) {  
          //TODO: Send push notification for this nutritionists  
          console.log("nutritionist FCM-Token: " + childSnapshot.val().fcm_token); 
  
            // Send notifications to all tokens.
            // return admin.messaging().sendToDevice(childSnapshot.val().fcm_token, payload)
        });  
    return true;
  });
});

 //Add a Nutritionist professional to the Newers List
exports.addNutritionistToNewerList = 
functions.database.ref('/nutritionists/{nutritionistId}').onWrite(event => {
  
  const snapshot = event.data;

  // Only edit data when it is first created.
  if (snapshot.previous.exists()) {
    console.log("{addNutritionitToNewerList}: Data already exists!"); 
    return;
  }
  // Exit when the data is deleted.
  if (!snapshot.exists()) {
    console.log("{addNutritionitToNewerList}: Data is deleted!");  
    return;
  }

  const id = snapshot.key;

  const payload = {
    id: `${id}`
  };
 console.log("snapshot id: " + id);  
 
 return admin.database().ref('nutritionistNewersList').push(payload);
});